% directSTNRecording 2019 analysis
% Figure 6 Pearson's correlation of beta with therapeutic current,
% therapeutic window
% Rev 01, Sept 2019
% Rev 02, Jan 2020

% PREREQUISITE run Figure 5 script first
load([dataDir '\TW.mat']); therapeuticWindows = TW([2 1 3],:); % reordering to antero-lat, medial, postero-lat
load([dataDir '\TC.mat']); therapeuticCurrents = TC([2 1 3],:); % reordering to antero-lat, medial, postero-lat

fh06 = figure('color','w');
set(fh06,'Units','centimeters','Position',[5 5 8.5 6.375])

rowSubplots = 2; 
colSubplots = 2;
spacingHor = 0.08;
spacingVer = 0.12;
marginRight = 0.04;
marginLeft = 0.08;
marginBottom = 0.15;
barFaceColorBeta = [42 42 42]/255;
barFaceColorBLow = [110 110 110]/255;
barFaceColorBHigh = [164 164 164]/255;
barFaceColorBPeak = [207 207 207]/255;
betaColors = [barFaceColorBeta; barFaceColorBLow; barFaceColorBHigh; barFaceColorBPeak];

directionRanking = [1 2 3; 1 3 2; 2 1 3; 2 1 3]; 
therapeuticCurrentsOriginal = therapeuticCurrents(:,[1 3:end]); % remove patient 9bis
therapeuticWindowsOriginal = therapeuticWindows(:,[1 3:end]);
therapeuticCurrents = zeros(length(patientNo),3);
therapeuticWindows = zeros(length(patientNo),3);
for pIdx = 1:length(patientNo)
    therapeuticCurrents(pIdx,:) = therapeuticCurrentsOriginal(directionRanking(pIdx,:),pIdx)'./max(therapeuticCurrentsOriginal(:,pIdx));
    therapeuticWindows(pIdx,:) = therapeuticWindowsOriginal(directionRanking(pIdx,:),pIdx)'./max(therapeuticWindowsOriginal(:,pIdx));
end

rhoCurrent = zeros(1,4);
pvalCurrent = zeros(1,4);
subplot(121)
for bIdx = 1:4 % frequency band index
    X = compiledMatrixResting(:,:,bIdx);
    Y = therapeuticCurrents;
    [rhoCurrent(bIdx),pvalCurrent(bIdx)] = corr(X(:),Y(:),'type','Spearman');
    plot(X,Y,'o','markeredgecolor',betaColors(bIdx,:),'markersize',4,'markerfacecolor',betaColors(bIdx,:))
    hold on
    xlabel('\beta relative power')
    ylabel('Normalized therapeutic current')
%     title(sprintf('Rho %1.2f, p %1.3f',rho(bIdx),pval(bIdx)))
end

rhoWindow = zeros(1,4);
pvalWindow = zeros(1,4);
subplot(122)
for bIdx = 1:4 % frequency band index
    X = compiledMatrixResting(:,:,bIdx);
    Y = log(therapeuticWindows);
    [rhoWindow(bIdx),pvalWindow(bIdx)] = corr(X(:),Y(:),'type','Spearman');
    plot(X,Y,'o','markeredgecolor',betaColors(bIdx,:),'markersize',4,'markerfacecolor',betaColors(bIdx,:))
    hold on
    xlabel('\beta relative power')
    ylabel('Normalized therapeutic window')
%     title(sprintf('Rho %1.2f, p %1.3f',rho(bIdx),pval(bIdx)))
end

fh07 = figure('color','w');
set(fh07,'Units','centimeters','Position',[5 5 8.5 6.375])

ax = gca; ax.FontSize = 8;
bIdx = 1;
X = compiledMatrixResting(:,:,bIdx);
Y = therapeuticWindows;
plot(X(:),Y(:),'o','markeredgecolor',betaColors(bIdx,:),'markersize',4,'markerfacecolor',betaColors(bIdx,:))
hold on
bRegression = [ones(length(X(:)),1) X(:)]\Y(:);
Yregression = [ones(length(X(:)),1) X(:)]*bRegression;
p1 = plot(X(:),Yregression,'--','linewidth',1.5,'color',betaColors(bIdx,:));

bIdx = 3;
X = compiledMatrixResting(:,:,bIdx);
plot(X(:),Y(:),'o','markeredgecolor',betaColors(bIdx,:),'markersize',4,'markerfacecolor',betaColors(bIdx,:))
bRegression = [ones(length(X(:)),1) X(:)]\Y(:);
Yregression = [ones(length(X(:)),1) X(:)]*bRegression;
p2 = plot(X(:),Yregression,'--','linewidth',1.5,'color',betaColors(bIdx,:));
legend([p1 p2],{'\beta' '\beta_{high}'},'location','southeast','orientation','horizontal')
legend('boxoff')

% title({'Correlation \beta power','and therapeutic window'})
xlabel('Relative power')
ylabel({'Normalized','therapeutic window'})

yticks(.2:.2:1.2)
yticklabels({'0.2' '' '0.8' '' '1'})
xticks(0:.3:1.8)
xticklabels({'0' '' '0.6' '' '1.2' '' '' })
set(gca,'TickDir','out');
box off

%     title(sprintf('Rho %1.2f, p %1.3f',rho(bIdx),pval(bIdx)))

%%
% if doPrint
%     print(fh07,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig06CorrelationRev02'),'-dtiffn','-r300')
% end
