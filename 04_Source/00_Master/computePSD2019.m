function [psd, f, psdCI] =  computePSD2019(data, freq_interest, Fs, fres)

numDirection   = size(data,2); % Number of directions
% N               = Fs/fres; % Number of samples used for hanning window
segmentLength = Fs;
noOverlap = Fs/5;
psd = zeros(length(freq_interest), numDirection);
f = zeros(length(freq_interest), numDirection);
psdCI = zeros(length(freq_interest), numDirection*2);
% Compute PSD
for directionInd = 1:numDirection
    %     [psd(:, directionInd), f(:, directionInd),psdCI(:,(directionInd)*2-1:(directionInd)*2)] = pwelch(data(:,directionInd), hamming(N), [], freq_interest, Fs,'ConfidenceLevel',0.95);
    % 2019
    [psd(:, directionInd), f(:, directionInd),psdCI(:,(directionInd)*2-1:(directionInd)*2)] = pwelch(data(:,directionInd), segmentLength, noOverlap, freq_interest, Fs,'ConfidenceLevel',0.31);
end
end