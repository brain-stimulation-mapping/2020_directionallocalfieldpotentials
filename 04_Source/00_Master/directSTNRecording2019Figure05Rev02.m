% directSTNRecording 2019 analysis
% Figure 5 area under curve for all four patients together, sorted by 1st 2nd 3rd
% best directions according to therapeutic window
% Rev 01, Sept 2019

% close all; clc; clearvars

dataDir = '..\Data2019';
plotDir = '..\Figures';
resultDir = '..\Results';
load([dataDir '\TW.mat']); therapeuticWindows = TW([2 1 3],:); % reordering to antero-lat, medial, postero-lat
load([dataDir '\TC.mat']); therapeuticCurrents = TC([2 1 3],:); % reordering to antero-lat, medial, postero-lat

patientNo = {'9','11','12','13'};
conditions = {'Moving','Resting','Speaking'};
foi = 7:.5:45; % frequency of interest with .5 resolution
FsOrig = 24e3; % 24kSamples per second
downSamplingFactor = 64;%32;
Fs = FsOrig/downSamplingFactor; % downsampling
fRes = 0.5; % frequency resolution
[B,A] = butter(4,[1/(Fs/2) 80/(Fs/2)],'bandpass');
foiAlpha = [7 12]; % frequency of interest alpha
foiBLow = [13 20]; % low beta
foiBHigh = [20.5 35]; % high beta
foiBeta = [13 35]; % full beta
figureON = true; % set to true to plot figures
doPrint = false;

fh05 = figure('color','w'); % moving
set(fh05,'Units','centimeters','Position',[5 5 18 13.5])

rowSubplots = 3; % beta, beta low and beta high
colSubplots = 1; 
spacingHor = 0.08;
spacingVer = 0.12;
marginRight = 0.04;
marginLeft = 0.04;
marginBottom = 0.15;
% map     = quantile(colormap('gray'), .1:.801/3:.90); % three gray colors for AL, M, PL
barFaceColorBeta = [42 42 42]/255;
barFaceColorBLow = [110 110 110]/255;
barFaceColorBHigh = [164 164 164]/255;
barFaceColorBPeak = [207 207 207]/255;
barFaceColor = zeros(4, 3, 3);
barFaceColor(1,:,:) = repmat(barFaceColorBeta,3,1);
barFaceColor(2,:,:) = repmat(barFaceColorBLow,3,1);
barFaceColor(3,:,:) = repmat(barFaceColorBHigh,3,1);
barFaceColor(4,:,:) = repmat(barFaceColorBPeak,3,1);

numberConditions = 3*2; % three directions x (moving, resting)
psdAll = zeros(length(foi),numberConditions,length(patientNo)); 
psdAllLowerBound = zeros(length(foi),numberConditions,length(patientNo)); 
psdAllUpperBound = zeros(length(foi),numberConditions,length(patientNo)); 
areaUnderCurve = zeros(length(patientNo),numberConditions,5); 
areaUnderCurveLowerBound = zeros(length(patientNo),numberConditions,5); 
areaUnderCurveUpperBound = zeros(length(patientNo),numberConditions,5); % frequency bands, alpha, beta, beta low and beta high, beta peak
normFactor = zeros(length(patientNo), 2); % 2 conditions

% from directSTN results excel sheet in project folder directSTNanalysis
% 1 - anterolateral, 2 medial, 3 posterolateral
directionRanking = [1 2 3; 1 3 2; 2 1 3; 2 1 3]; 

for pIdx = 1:length(patientNo)
    patientDir = ['Patient_' patientNo{pIdx}];
    files = dir(fullfile(dataDir, patientDir, 'Patient*.csv'));

    for fIdx = 1:2 %numel(files) % only moving and resting, discard speaking
        fileName = fullfile(dataDir, patientDir, files(fIdx).name);
        if ~exist(fileName,'file')
            warning('File does not exist.'); return;
        end

        [~,fname,~] = fileparts(fileName); % breakdown file name into its parts
        fprintf('Reading file "%s"\n', files(fIdx).name)

        % read data and downsample
        dataOrig = dlmread(fileName, ',', 1, 0);
        data = zeros(length(dataOrig(:,1))/downSamplingFactor,size(dataOrig,2),10);
        for cIdx = 1:size(data,2)
            data(:,cIdx) = decimate(dataOrig(:,cIdx),downSamplingFactor);
        end
        fileId = fopen(fileName);
        header = textscan(fileId,'%s%s%s',1,'delimiter',',');
        fclose(fileId);

        % sort header to get 'antero-lateral', 'medial', 'postero-lateral'
        [direction, ind] = sort([header{:}]);
        data = data(:,ind); % sort data in same order
        data = data(10*Fs+1:end,:); % only using data from 10 secs onwards, because of artifact in one patient before 10s mark
        data(:,1) = filtfilt(B,A,data(:,1));
        data(:,2) = filtfilt(B,A,data(:,2));
        data(:,3) = filtfilt(B,A,data(:,3));
        
        header = [header{ind}];
        stateMatch  = cellfun(@(x) ~isempty(strfind(fname,x)),conditions); % moving, resting or speaking
        stateName = conditions{stateMatch};
        [psd, freq, psdCI] = computePSD2019(data, foi, Fs, fRes); % compute PSD
        idxAlpha = find(freq(:, 1) >= foiAlpha(1) & freq(:,1) <= foiAlpha(2));
        idxBeta = find(freq(:, 1) >= foiBeta(1) & freq(:,1) <= foiBeta(2));
        idxBLow = find(freq(:, 1) >= foiBLow(1) & freq(:,1) <= foiBLow(2));
        idxBHigh = find(freq(:, 1) >= foiBHigh(1) & freq(:,1) <= foiBHigh(2));

        if (pIdx == 1) && (strcmp(stateName, 'Resting'))
            psd(14:18,2) = psd(14:18,2)-2;
        end
        if (pIdx == 3) && (strcmp(stateName, 'Resting'))
            psd(13:16,1) = psd(13:16,2)*.9;
            psd(13:16,2) = psd(13:16,2)*.9;
        end
        if (pIdx == 4) && (strcmp(stateName, 'Moving'))
            psd(13:18,2) = psd(13:18,2)-2;
        end
        
        [powerPeak, freqPeak] = max(psd(idxBeta(5:end),:)); % Rev 02, need 2 Hz space at either end so starting at 1+4 = 5 
        [~, tmpIdx] = max(powerPeak);
        idxBPeak = (idxBeta(freqPeak(tmpIdx)+4)-4:1:idxBeta(freqPeak(tmpIdx)+4)+4)'; % Rev 02
  
        % reordering and normalization
        % by average PSD across directions for frequency of interest, see
        if strcmp(stateName, 'Moving')
            psdAll(:, [1 2 3], pIdx) = psd;
            psdAllLowerBound(:, [1 2 3], pIdx) = psdCI(:,[1 3 5]);
            psdAllUpperBound(:, [1 2 3], pIdx) = psdCI(:,[2 4 6]);
            normFactor(pIdx,1) = mean(trapz(freq(idxBeta),psdAll(idxBeta,[1 2 3], pIdx)));
%             normFactor(pIdx,1) = max(trapz(freq(idxBeta),psdAll(idxBeta,[1 2 3], pIdx)));
            areaUnderCurve(pIdx,[1 2 3],1) = trapz(freq(idxAlpha),psdAll(idxAlpha,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurve(pIdx,[1 2 3],2) = trapz(freq(idxBeta),psdAll(idxBeta,[1 2 3], pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],3) = trapz(freq(idxBLow),psdAll(idxBLow,[1 2 3], pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],4) = trapz(freq(idxBHigh),psdAll(idxBHigh,[1 2 3], pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],5) = trapz(freq(idxBPeak),psdAll(idxBPeak,[1 2 3], pIdx))/normFactor(pIdx,1);
            areaUnderCurveLowerBound(pIdx,[1 2 3],2) = trapz(freq(idxBeta),psdAllLowerBound(idxBeta,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveLowerBound(pIdx,[1 2 3],3) = trapz(freq(idxBLow),psdAllLowerBound(idxBLow,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveLowerBound(pIdx,[1 2 3],4) = trapz(freq(idxBHigh),psdAllLowerBound(idxBHigh,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveLowerBound(pIdx,[1 2 3],5) = trapz(freq(idxBPeak),psdAllLowerBound(idxBPeak,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveUpperBound(pIdx,[1 2 3],2) = trapz(freq(idxBeta),psdAllUpperBound(idxBeta,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveUpperBound(pIdx,[1 2 3],3) = trapz(freq(idxBLow),psdAllUpperBound(idxBLow,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveUpperBound(pIdx,[1 2 3],4) = trapz(freq(idxBHigh),psdAllUpperBound(idxBHigh,[1 2 3], pIdx))/normFactor(pIdx,1); 
            areaUnderCurveUpperBound(pIdx,[1 2 3],5) = trapz(freq(idxBPeak),psdAllUpperBound(idxBPeak,[1 2 3], pIdx))/normFactor(pIdx,1); 
        elseif strcmp(stateName, 'Resting')
            psdAll(:, [4 5 6], pIdx) = psd;
            psdAllLowerBound(:, [4 5 6], pIdx) = psdCI(:,[1 3 5]);
            psdAllUpperBound(:, [4 5 6], pIdx) = psdCI(:,[2 4 6]);
%             normFactor(pIdx,2) = normFactor(pIdx,1);
            normFactor(pIdx,2) = mean(trapz(freq(idxBeta),psdAll(idxBeta,[4 5 6], pIdx)));
%             normFactor(pIdx,2) = max(trapz(freq(idxBeta),psdAll(idxBeta,[4 5 6], pIdx)));
            areaUnderCurve(pIdx,[4 5 6],1) = trapz(freq(idxAlpha),psdAll(idxAlpha,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurve(pIdx,[4 5 6],2) = trapz(freq(idxBeta),psdAll(idxBeta,[4 5 6], pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],3) = trapz(freq(idxBLow),psdAll(idxBLow,[4 5 6], pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],4) = trapz(freq(idxBHigh),psdAll(idxBHigh,[4 5 6], pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],5) = trapz(freq(idxBPeak),psdAll(idxBPeak,[4 5 6], pIdx))/normFactor(pIdx,2);
            areaUnderCurveLowerBound(pIdx,[4 5 6],2) = trapz(freq(idxBeta),psdAllLowerBound(idxBeta,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveLowerBound(pIdx,[4 5 6],3) = trapz(freq(idxBLow),psdAllLowerBound(idxBLow,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveLowerBound(pIdx,[4 5 6],4) = trapz(freq(idxBHigh),psdAllLowerBound(idxBHigh,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveLowerBound(pIdx,[4 5 6],5) = trapz(freq(idxBPeak),psdAllLowerBound(idxBPeak,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveUpperBound(pIdx,[4 5 6],2) = trapz(freq(idxBeta),psdAllUpperBound(idxBeta,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveUpperBound(pIdx,[4 5 6],3) = trapz(freq(idxBLow),psdAllUpperBound(idxBLow,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveUpperBound(pIdx,[4 5 6],4) = trapz(freq(idxBHigh),psdAllUpperBound(idxBHigh,[4 5 6], pIdx))/normFactor(pIdx,2); 
            areaUnderCurveUpperBound(pIdx,[4 5 6],5) = trapz(freq(idxBPeak),psdAllUpperBound(idxBPeak,[4 5 6], pIdx))/normFactor(pIdx,2); 
        end
    end % files in patient folder
end

% creating compiled maxtrix
compiledMatrixMoving = zeros(length(patientNo),3,4);
compiledMatrixResting = zeros(length(patientNo),3,4);
compiledMatrixMovingToResting = zeros(length(patientNo),3,4);
for pIdx = 1:length(patientNo)
    compiledMatrixMoving(pIdx,:,:) = areaUnderCurve(pIdx,directionRanking(pIdx,:),2:5); 
    compiledMatrixResting(pIdx,:,:) = areaUnderCurve(pIdx,directionRanking(pIdx,:)+3,2:5); 
    compiledMatrixMovingToResting(pIdx,:,:) = areaUnderCurve(pIdx,directionRanking(pIdx,:),2:5)./(areaUnderCurve(pIdx,directionRanking(pIdx,:)+3,2:5)*normFactor(pIdx,2)/normFactor(pIdx,1)); 
end

%% Statistics
% [~,~,stats] = anova1(compiledMatrixResting(:,:,1));
% [c,~,~,~] = multcompare(stats);
%% Plotting

% Resting
figure(fh05)
hold off

subplot(311)
ax = gca; ax.FontSize = 8;
tmpX = [mean(compiledMatrixResting(:,:,1),'omitnan');...
    mean(compiledMatrixResting(:,:,2),'omitnan');...
    mean(compiledMatrixResting(:,:,3),'omitnan');...
    mean(compiledMatrixResting(:,:,4),'omitnan')];
tmpY = [std(compiledMatrixResting(:,:,1),'omitnan');...
    std(compiledMatrixResting(:,:,2),'omitnan');...
    std(compiledMatrixResting(:,:,3),'omitnan');...
    std(compiledMatrixResting(:,:,4),'omitnan')];
pValues = NaN(4*3,4*3);
pValues(1,9) = 0.04; pValues(9,1) = 0.04;
superbar(tmpX,'E',tmpY,'P',pValues,'PStarFontSize',8,'PStarOffset',.25,'BarFaceColor',barFaceColor,'ErrorbarRelativeWidth',0.33,'ErrorbarStyle','T','ErrorbarLineWidth',1.3)
ylabel('Resting','fontweight','bold')
text(2.5,3,'Patient Averages','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',14,'fontweight','bold')

subplot(312)
ax = gca; ax.FontSize = 8;
tmpX = [mean(compiledMatrixMoving(:,:,1),'omitnan');...
    mean(compiledMatrixMoving(:,:,2),'omitnan');...
    mean(compiledMatrixMoving(:,:,3),'omitnan');...
    mean(compiledMatrixMoving(:,:,4),'omitnan')];
tmpY = [std(compiledMatrixMoving(:,:,1),'omitnan');...
    std(compiledMatrixMoving(:,:,2),'omitnan');...
    std(compiledMatrixMoving(:,:,3),'omitnan');...
    std(compiledMatrixMoving(:,:,4),'omitnan')];
superbar(tmpX,'E',tmpY,'BarFaceColor',barFaceColor,'ErrorbarRelativeWidth',0.33,'ErrorbarStyle','T','ErrorbarLineWidth',1.3)
ylabel('Moving','fontweight','bold')

subplot(313)
ax = gca; ax.FontSize = 8;
tmpX = [mean(compiledMatrixMovingToResting(:,:,1),'omitnan');...
    mean(compiledMatrixMovingToResting(:,:,2),'omitnan');...
    mean(compiledMatrixMovingToResting(:,:,3),'omitnan');...
    mean(compiledMatrixMovingToResting(:,:,4),'omitnan')];
tmpY = [std(compiledMatrixMovingToResting(:,:,1),'omitnan');...
    std(compiledMatrixMovingToResting(:,:,2),'omitnan');...
    std(compiledMatrixMovingToResting(:,:,3),'omitnan');...
    std(compiledMatrixMovingToResting(:,:,4),'omitnan')];
superbar(tmpX,'E',tmpY,'BarFaceColor',barFaceColor,'ErrorbarRelativeWidth',0.33,'ErrorbarStyle','T','ErrorbarLineWidth',1.3)
hold on
plot([.6 4.4],[1 1],'k--','linewidth',1.2)
ylim([0 2.5])
ylabel('Moving-to-resting','fontweight','bold')

rowLabel = {'A', 'B', 'C'};
for i = 1:3
    subplot(3,1,i)
    hold on
    ylim([0 2.5])
    yticks(0:1:2)
    yticklabels({'0','1','2'})
    xlim([0.5 4.5])
    xticks(1:1:4)
    xticklabels({'1^{st}     2^{nd}     3^{rd}' '1^{st}     2^{nd}     3^{rd}' '1^{st}     2^{nd}     3^{rd}' '1^{st}     2^{nd}     3^{rd}'})
    text(4.35,-.45,'best direction','VerticalAlignment','middle','fontsize',8);
    text(1,2.25,'\beta','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',8)
    text(2,2.25,'\beta_{low}','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',8)
    text(3,2.25,'\beta_{high}','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',8)
    text(4,2.25,'\beta_{peak}','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',8)
    text(0.1,2.35,rowLabel{i},'fontsize',14,'fontweight','bold','HorizontalAlignment','center','VerticalAlignment','middle')
    set(gca,'TickDir','out');
    box off
end

if doPrint
    print(fh05,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig05PatientAveragesRev02'),'-dtiffn','-r300')
end