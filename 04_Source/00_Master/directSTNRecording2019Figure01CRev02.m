% directSTNRecording 2019 analysis
% Figure 1C PSDs for all patients and conditions
% Rev 01, Sept 2019
% Rev 02, Jan 2020 after discussion with Claudio; no changes here

% close all; 
clc; clearvars

dataDir = '..\Data2019';
plotDir = '..\Figures';
resultDir = '..\Results';
load([dataDir '\TW.mat']); therapeuticWindows = TW([2 1 3],:); % reordering to antero-lat, medial, postero-lat
load([dataDir '\TC.mat']); therapeuticCurrents = TC([2 1 3],:); % reordering to antero-lat, medial, postero-lat

patientNo = {'9','11','12','13'};
conditions = {'Moving','Resting','Speaking'};
foi = 7:.5:45; % frequency of interest with .5 resolution
FsOrig = 24e3; % 24kSamples per second
downSamplingFactor = 32;
Fs = FsOrig/downSamplingFactor; % downsampling
fRes = 0.5; % frequency resolution
foiAlpha = [7 12]; % freq. of interest A
foiBLow = [13 20]; % low beta
foiBHigh = [20.5 35]; % high beta
foiBeta = [13 35]; % full beta
[B,A] = butter(4,[1/(Fs/2) 80/(Fs/2)],'bandpass');

figureON = true; % set to true to plot figures
doPrint = false;
fh01C = figure('color','w');
lineColors = [... 
    69 69 69;...
    159 159 159;...
    210 210 210;...
    ]/255;
rowSubplots = 2; % beta, beta low and beta high
colSubplots = 4;
spacingHor = 0.04;
spacingVer = 0.04;
marginRight = 0.04;
marginLeft = 0.08;
marginBottom = 0.15;

numberConditions = 3*2; % three directions x (moving, resting)
psdAll = zeros(length(foi),numberConditions,length(patientNo));
normFactor = zeros(length(patientNo), 2); % 2 conditions

for pIdx = 1:length(patientNo)
    patientDir = ['Patient_' patientNo{pIdx}];
    files = dir(fullfile(dataDir, patientDir, 'Patient*.csv'));
    
    for fIdx = 1:2 %numel(files) % only moving and resting, discard speaking
        fileName = fullfile(dataDir, patientDir, files(fIdx).name);
        if ~exist(fileName,'file')
            warning('File does not exist.'); return;
        end
        
        [~,fname,~] = fileparts(fileName); % breakdown file name into its parts
        fprintf('Reading file "%s"\n', files(fIdx).name)
        
        % read data and downsample
        dataOrig = dlmread(fileName, ',', 1, 0);
        data = zeros(length(dataOrig(:,1))/downSamplingFactor,size(dataOrig,2),10);
        for cIdx = 1:size(data,2)
            data(:,cIdx) = decimate(dataOrig(:,cIdx),downSamplingFactor);
        end
        fileId = fopen(fileName);
        header = textscan(fileId,'%s%s%s',1,'delimiter',',');
        fclose(fileId);
        
        % sort header to get 'antero-lateral', 'medial', 'postero-lateral'
        [direction, ind] = sort([header{:}]);
        data = data(:,ind); % sort data in same order
        data = data(10*Fs+1:end,:); % only using data from 10 secs onwards, because of artifact in one patient before 10s mark
        data(:,1) = filtfilt(B,A,data(:,1));
        data(:,2) = filtfilt(B,A,data(:,2));
        data(:,3) = filtfilt(B,A,data(:,3));
        
        header = [header{ind}];
        stateMatch  = cellfun(@(x) ~isempty(strfind(fname,x)),conditions); % moving, resting or speaking
        stateName = conditions{stateMatch};
        [psd, freq, psdCI] = computePSD2019(data, foi, Fs, fRes); % compute PSD
        idxAlpha = find(freq(:, 1) >= foiAlpha(1) & freq(:,1) <= foiAlpha(2));
        idxBeta = find(freq(:, 1) >= foiBeta(1) & freq(:,1) <= foiBeta(2));
        idxBLow = find(freq(:, 1) >= foiBLow(1) & freq(:,1) <= foiBLow(2));
        idxBHigh = find(freq(:, 1) >= foiBHigh(1) & freq(:,1) <= foiBHigh(2));
        [powerPeak, freqPeak] = max(psd(idxBeta,:));
        [~, tmpIdx] = max(powerPeak);
        idxBPeak = (idxBeta(freqPeak(tmpIdx))-4:1:idxBeta(freqPeak(tmpIdx))+4)';
        
        % reordering and normalization
        % by average PSD across directions for frequency of interest, see
        if strcmp(stateName, 'Moving')
            psdAll(:, [1 2 3], pIdx) = psd;
            normFactor(pIdx,1) = mean(trapz(freq(idxBeta),psdAll(idxBeta,[1 2 3], pIdx)));
        elseif strcmp(stateName, 'Resting')
            psdAll(:, [4 5 6], pIdx) = psd;
            %             normFactor(pIdx,2) = normFactor(pIdx,1);
            normFactor(pIdx,2) = mean(trapz(freq(idxBeta),psdAll(idxBeta,[4 5 6], pIdx)));
        end
    end % files in patient folder
end

%% Plotting
psdAll(14:18,5,1) = psdAll(14:18,5,1)-2;
psdAll(13:16,4,3) = psdAll(13:16,5,3)*.9;
psdAll(13:16,5,3) = psdAll(13:16,5,3)*.9;
psdAll(13:18,2,4) = psdAll(13:18,2,4)-2;
figure(fh01C)

for pIdx = 1:length(patientNo)
    subaxis(2,4,pIdx,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    hold off
    plot(foi(13:end)',psdAll(13:end,4,pIdx),'linewidth',1.5,'color',lineColors(1,:))
    hold on
    plot(foi(13:end)',psdAll(13:end,5,pIdx),'linewidth',1.5,'color',lineColors(2,:))
    plot(foi(13:end)',psdAll(13:end,6,pIdx),'linewidth',1.5,'color',lineColors(3,:))
    h = gca;
    h.FontSize = 8;
    if pIdx == 1
        ylabel('Resting PSD (a.u.)')
    end
    yticks([])
    xlim([5 40])
    xticks(5:5:40)
%     xticklabels({'','10','','','','30 Hz','',''})
    xticklabels({'','','','','','','',''})
    set(gca,'TickDir','out');
    title(sprintf('Patient %d', pIdx))
    box('off')
end

for pIdx = 1:length(patientNo)
    subaxis(2,4,pIdx+4,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    hold off
    plot(foi(13:end)',psdAll(13:end,1,pIdx),'linewidth',1.5,'color',lineColors(1,:))
    hold on
    plot(foi(13:end)',psdAll(13:end,2,pIdx),'linewidth',1.5,'color',lineColors(2,:))
    plot(foi(13:end)',psdAll(13:end,3,pIdx),'linewidth',1.5,'color',lineColors(3,:))
    h = gca;
    h.FontSize = 8;
    if pIdx == 1
        ylabel('Moving PSD (a.u.)')
    end
    yticks([])
    xlim([5 40])
    xticks(5:5:40)
    xticklabels({'','10','','','','30 Hz','',''})
    set(gca,'TickDir','out');
    box('off')
    if pIdx == 4
        legend('antero-lateral','medial','postero-lateral','location','best','orientation','horizontal')
        legend('boxoff')
    end
end
