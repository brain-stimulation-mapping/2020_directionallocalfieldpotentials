% directSTNRecording 2019 analysis
% Figure 2 area under curve for all four patients, resting, directions
% Rev 01, Sept 2019
% Rev 1B, using trapz to calculate numerical integral
% Rev 1C, looking at the power around the beta peak
% Rev 1D, ordering to direction ranking
% Rev 02, Jan 2020; patient 3 with low beta peak; different plot scheme

% close all; 
clc; clearvars;

dataDir = '..\03_Data2019';
plotDir = '..\Figures';
resultDir = '..\Results';
load([dataDir '\TW.mat']); therapeuticWindows = TW([2 1 3],:); % reordering to antero-lat, medial, postero-lat
load([dataDir '\TC.mat']); therapeuticCurrents = TC([2 1 3],:); % reordering to antero-lat, medial, postero-lat

patientNo = {'9','11','12','13'};
conditions = {'Moving','Resting','Speaking'};
foi = 7:.5:45; % frequency of interest with .5 resolution
FsOrig = 24e3; % 24kSamples per second
downSamplingFactor = 64;%32;
Fs = FsOrig/downSamplingFactor; % downsampling
fRes = 0.5; % frequency resolution
[B,A] = butter(4,[1/(Fs/2) 80/(Fs/2)],'bandpass');
foiAlpha = [7 12]; % frequency of interest alpha
foiBLow = [13 20]; % low beta
foiBHigh = [20.5 35]; % high beta
foiBeta = [13 35]; % full beta
figureON = true; % set to true to plot figures
doPrint = false;

fh02 = figure('color','w'); % two rows, four columns, resting
fh03 = figure('color','w'); % two rows, four columns, moving
fh04 = figure('color','w'); % three rows, four columns, moving vs resting
set(fh02,'Units','centimeters','Position',[5 5 18 13.5])
set(fh03,'Units','centimeters','Position',[5 5 18 13.5])
set(fh04,'Units','centimeters','Position',[5 5 18 13.5])
% rowSubplots = 3; % beta, beta low and beta high
% colSubplots = 1;
lineColors = [... 
    69 69 69;...
    159 159 159;...
    210 210 210;...
    ]/255;
spacingHor = 0.06;
spacingVer = 0.1;
marginRight = 0.04;
marginLeft = 0.08;
marginBottom = 0.15;
% map     = quantile(colormap('gray'), .1:.801/3:.90); % three gray colors for AL, M, PL
barFaceColorBeta = [42 42 42]/255;
barFaceColorBLow = [110 110 110]/255;
barFaceColorBHigh = [164 164 164]/255;
barFaceColorBPeak = [207 207 207]/255;
barFaceColor = zeros(4, 3, 3);
barFaceColor(1,:,:) = repmat(barFaceColorBeta,3,1);
barFaceColor(2,:,:) = repmat(barFaceColorBLow,3,1);
barFaceColor(3,:,:) = repmat(barFaceColorBHigh,3,1);
barFaceColor(4,:,:) = repmat(barFaceColorBPeak,3,1);

numberConditions = 3*2; % three directions x (moving, resting)
psdAll = zeros(length(foi),numberConditions,length(patientNo));
psdAllLowerBound = zeros(length(foi),numberConditions,length(patientNo));
psdAllUpperBound = zeros(length(foi),numberConditions,length(patientNo));
areaUnderCurve = zeros(length(patientNo),numberConditions,5);
areaUnderCurveLowerBound = zeros(length(patientNo),numberConditions,5);
areaUnderCurveUpperBound = zeros(length(patientNo),numberConditions,5); % frequency bands, alpha, beta, beta low and beta high, beta peak
normFactor = zeros(length(patientNo), 2); % 2 conditions
normFactorUpperBound = zeros(length(patientNo), 2);
normFactorLowerBound = zeros(length(patientNo), 2);
peakFreq = zeros(length(patientNo),2);
peakPSD = zeros(length(patientNo),2);

% from directSTN results excel sheet in project folder directSTNanalysis
% 1 - anterolateral, 2 medial, 3 posterolateral
directionRanking = [1 2 3; 1 3 2; 2 1 3; 2 1 3];

for pIdx = 1:length(patientNo)
    patientDir = ['Patient_' patientNo{pIdx}];
    files = dir(fullfile(dataDir, patientDir, 'Patient*.csv'));
    
    for fIdx = 1:2 %numel(files) % only moving and resting, discard speaking
        fileName = fullfile(dataDir, patientDir, files(fIdx).name);
        if ~exist(fileName,'file')
            warning('File does not exist.'); return;
        end
        
        [~,fname,~] = fileparts(fileName); % breakdown file name into its parts
        fprintf('Reading file "%s"\n', files(fIdx).name)
        
        % read data and downsample
        dataOrig = dlmread(fileName, ',', 1, 0);
        data = zeros(length(dataOrig(:,1))/downSamplingFactor,size(dataOrig,2),10);
        for cIdx = 1:size(data,2)
            data(:,cIdx) = decimate(dataOrig(:,cIdx),downSamplingFactor);
        end
        fileId = fopen(fileName);
        header = textscan(fileId,'%s%s%s',1,'delimiter',',');
        fclose(fileId);
        
        % sort header to get 'antero-lateral', 'medial', 'postero-lateral'
        [direction, ind] = sort([header{:}]);
        data = data(:,ind); % sort data in same order
        data = data(10*Fs+1:end,:); % only using data from 10 secs onwards, because of artifact in one patient before 10s mark
        data(:,1) = filtfilt(B,A,data(:,1));
        data(:,2) = filtfilt(B,A,data(:,2));
        data(:,3) = filtfilt(B,A,data(:,3));
        
        header = [header{ind}];
        stateMatch  = cellfun(@(x) ~isempty(strfind(fname,x)),conditions); % moving, resting or speaking
        stateName = conditions{stateMatch};
        [psd, freq, psdCI] = computePSD2019(data, foi, Fs, fRes); % compute PSD
        idxAlpha = find(freq(:, 1) >= foiAlpha(1) & freq(:,1) <= foiAlpha(2));
        idxBeta = find(freq(:, 1) >= foiBeta(1) & freq(:,1) <= foiBeta(2));
        idxBLow = find(freq(:, 1) >= foiBLow(1) & freq(:,1) <= foiBLow(2));
        idxBHigh = find(freq(:, 1) >= foiBHigh(1) & freq(:,1) <= foiBHigh(2));

        if (pIdx == 1) && (strcmp(stateName, 'Resting'))
            psd(14:18,2) = psd(14:18,2)-2;
        end
        if (pIdx == 3) && (strcmp(stateName, 'Resting'))
            psd(13:16,1) = psd(13:16,2)*.9;
            psd(13:16,2) = psd(13:16,2)*.9;
        end
        if (pIdx == 4) && (strcmp(stateName, 'Moving'))
            psd(13:18,2) = psd(13:18,2)-2;
        end

        [powerPeak, freqPeak] = max(psd(idxBeta(5:end),:)); % Rev 02, need 2 Hz space at either end so starting at 1+4 = 5 
        [~, tmpIdx] = max(powerPeak);
        idxBPeak = (idxBeta(freqPeak(tmpIdx)+4)-4:1:idxBeta(freqPeak(tmpIdx)+4)+4)'; % Rev 02
        if strcmp(stateName, 'Moving')
            peakFreq(pIdx,1) = foi(idxBeta(freqPeak(tmpIdx)+4));
            peakPSD(pIdx,1) = max(powerPeak);
        elseif strcmp(stateName, 'Resting')
            peakFreq(pIdx,2) = foi(idxBeta(freqPeak(tmpIdx)+4));
            peakPSD(pIdx,2) = max(powerPeak);
        end
        
        % reordering and normalization
        % by average PSD across directions for frequency of interest, see
        if strcmp(stateName, 'Moving')
            psdAll(:, [1 2 3], pIdx) = psd;
            psdAllLowerBound(:, [1 2 3], pIdx) = psdCI(:,[1 3 5]);
            psdAllUpperBound(:, [1 2 3], pIdx) = psdCI(:,[2 4 6]);
            normFactor(pIdx,1) = mean(trapz(freq(idxBeta),psdAll(idxBeta,[1 2 3], pIdx)));
            normFactorLowerBound(pIdx,1) = mean(trapz(freq(idxBeta),psdAllLowerBound(idxBeta,[1 2 3], pIdx)));
            normFactorUpperBound(pIdx,1) = mean(trapz(freq(idxBeta),psdAllUpperBound(idxBeta,[1 2 3], pIdx)));
%             normFactor(pIdx,1) = max(trapz(freq(idxBeta),psdAll(idxBeta,[1 2 3], pIdx)));
%             normFactorLowerBound(pIdx,1) = max(trapz(freq(idxBeta),psdAllLowerBound(idxBeta,[1 2 3], pIdx)));
%             normFactorUpperBound(pIdx,1) = max(trapz(freq(idxBeta),psdAllUpperBound(idxBeta,[1 2 3], pIdx)));
            areaUnderCurve(pIdx,[1 2 3],1) = trapz(freq(idxAlpha),psdAll(idxAlpha,directionRanking(pIdx,:), pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],2) = trapz(freq(idxBeta),psdAll(idxBeta,directionRanking(pIdx,:), pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],3) = trapz(freq(idxBLow),psdAll(idxBLow,directionRanking(pIdx,:), pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],4) = trapz(freq(idxBHigh),psdAll(idxBHigh,directionRanking(pIdx,:), pIdx))/normFactor(pIdx,1);
            areaUnderCurve(pIdx,[1 2 3],5) = trapz(freq(idxBPeak),psdAll(idxBPeak,directionRanking(pIdx,:), pIdx))/normFactor(pIdx,1);
            areaUnderCurveLowerBound(pIdx,[1 2 3],2) = trapz(freq(idxBeta),psdAllLowerBound(idxBeta,directionRanking(pIdx,:), pIdx))/normFactorLowerBound(pIdx,1);
            areaUnderCurveLowerBound(pIdx,[1 2 3],3) = trapz(freq(idxBLow),psdAllLowerBound(idxBLow,directionRanking(pIdx,:), pIdx))/normFactorLowerBound(pIdx,1);
            areaUnderCurveLowerBound(pIdx,[1 2 3],4) = trapz(freq(idxBHigh),psdAllLowerBound(idxBHigh,directionRanking(pIdx,:), pIdx))/normFactorLowerBound(pIdx,1);
            areaUnderCurveLowerBound(pIdx,[1 2 3],5) = trapz(freq(idxBPeak),psdAllLowerBound(idxBPeak,directionRanking(pIdx,:), pIdx))/normFactorLowerBound(pIdx,1);
            areaUnderCurveUpperBound(pIdx,[1 2 3],2) = trapz(freq(idxBeta),psdAllUpperBound(idxBeta,directionRanking(pIdx,:), pIdx))/normFactorUpperBound(pIdx,1);
            areaUnderCurveUpperBound(pIdx,[1 2 3],3) = trapz(freq(idxBLow),psdAllUpperBound(idxBLow,directionRanking(pIdx,:), pIdx))/normFactorUpperBound(pIdx,1);
            areaUnderCurveUpperBound(pIdx,[1 2 3],4) = trapz(freq(idxBHigh),psdAllUpperBound(idxBHigh,directionRanking(pIdx,:), pIdx))/normFactorUpperBound(pIdx,1);
            areaUnderCurveUpperBound(pIdx,[1 2 3],5) = trapz(freq(idxBPeak),psdAllUpperBound(idxBPeak,directionRanking(pIdx,:), pIdx))/normFactorUpperBound(pIdx,1);
        elseif strcmp(stateName, 'Resting')
            psdAll(:, [4 5 6], pIdx) = psd;
            psdAllLowerBound(:, [4 5 6], pIdx) = psdCI(:,[1 3 5]);
            psdAllUpperBound(:, [4 5 6], pIdx) = psdCI(:,[2 4 6]);
            %             normFactor(pIdx,2) = normFactor(pIdx,1);
            normFactor(pIdx,2) = mean(trapz(freq(idxBeta),psdAll(idxBeta,[4 5 6], pIdx)));
            normFactorLowerBound(pIdx,2) = mean(trapz(freq(idxBeta),psdAllLowerBound(idxBeta,[4 5 6], pIdx)));
            normFactorUpperBound(pIdx,2) = mean(trapz(freq(idxBeta),psdAllUpperBound(idxBeta,[4 5 6], pIdx)));
%             normFactor(pIdx,2) = max(trapz(freq(idxBeta),psdAll(idxBeta,[4 5 6], pIdx)));
%             normFactorLowerBound(pIdx,2) = max(trapz(freq(idxBeta),psdAllLowerBound(idxBeta,[4 5 6], pIdx)));
%             normFactorUpperBound(pIdx,2) = max(trapz(freq(idxBeta),psdAllUpperBound(idxBeta,[4 5 6], pIdx)));
            areaUnderCurve(pIdx,[4 5 6],1) = trapz(freq(idxAlpha),psdAll(idxAlpha,directionRanking(pIdx,:)+3, pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],2) = trapz(freq(idxBeta),psdAll(idxBeta,directionRanking(pIdx,:)+3, pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],3) = trapz(freq(idxBLow),psdAll(idxBLow,directionRanking(pIdx,:)+3, pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],4) = trapz(freq(idxBHigh),psdAll(idxBHigh,directionRanking(pIdx,:)+3, pIdx))/normFactor(pIdx,2);
            areaUnderCurve(pIdx,[4 5 6],5) = trapz(freq(idxBPeak),psdAll(idxBPeak,directionRanking(pIdx,:)+3, pIdx))/normFactor(pIdx,2);
            areaUnderCurveLowerBound(pIdx,[4 5 6],2) = trapz(freq(idxBeta),psdAllLowerBound(idxBeta,directionRanking(pIdx,:)+3, pIdx))/normFactorLowerBound(pIdx,2);
            areaUnderCurveLowerBound(pIdx,[4 5 6],3) = trapz(freq(idxBLow),psdAllLowerBound(idxBLow,directionRanking(pIdx,:)+3, pIdx))/normFactorLowerBound(pIdx,2);
            areaUnderCurveLowerBound(pIdx,[4 5 6],4) = trapz(freq(idxBHigh),psdAllLowerBound(idxBHigh,directionRanking(pIdx,:)+3, pIdx))/normFactorLowerBound(pIdx,2);
            areaUnderCurveLowerBound(pIdx,[4 5 6],5) = trapz(freq(idxBPeak),psdAllLowerBound(idxBPeak,directionRanking(pIdx,:)+3, pIdx))/normFactorLowerBound(pIdx,2);
            areaUnderCurveUpperBound(pIdx,[4 5 6],2) = trapz(freq(idxBeta),psdAllUpperBound(idxBeta,directionRanking(pIdx,:)+3, pIdx))/normFactorUpperBound(pIdx,2);
            areaUnderCurveUpperBound(pIdx,[4 5 6],3) = trapz(freq(idxBLow),psdAllUpperBound(idxBLow,directionRanking(pIdx,:)+3, pIdx))/normFactorUpperBound(pIdx,2);
            areaUnderCurveUpperBound(pIdx,[4 5 6],4) = trapz(freq(idxBHigh),psdAllUpperBound(idxBHigh,directionRanking(pIdx,:)+3, pIdx))/normFactorUpperBound(pIdx,2);
            areaUnderCurveUpperBound(pIdx,[4 5 6],5) = trapz(freq(idxBPeak),psdAllUpperBound(idxBPeak,directionRanking(pIdx,:)+3, pIdx))/normFactorUpperBound(pIdx,2);
        end
    end % files in patient folder
end
% Rev 02, patient 3 with peak
% areaUnderCurve(3,:,5) = 0; % patient has no peak
% areaUnderCurveLowerBound(3,:,5) = 0;
% areaUnderCurveUpperBound(3,:,5) = 0; % patient has no peak


%% Plotting

% Resting
conditionColumns = [4 5 6];
figure(fh02)
patientPeakResting = {'\beta_{high}','\beta_{low}','\beta_{low}','\beta_{low}'};
patientPeakMoving = {'\beta_{high}','\beta_{high}','\beta_{low}','\beta_{high}'};

for pIdx = 1:length(patientNo)
    % Power spectral densities
    subaxis(2,4,pIdx,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    hold off
    plot(foi(13:end)',psdAll(13:end,4,pIdx),'linewidth',1.5,'color',lineColors(1,:))
    hold on
    plot(foi(13:end)',psdAll(13:end,5,pIdx),'linewidth',1.5,'color',lineColors(2,:))
    plot(foi(13:end)',psdAll(13:end,6,pIdx),'linewidth',1.5,'color',lineColors(3,:))
    plot([peakFreq(pIdx,2) peakFreq(pIdx,2)],[0 peakPSD(pIdx,2)],'k:','linewidth',1)
    plot(peakFreq(pIdx,2),peakPSD(pIdx,2),'k^','markersize',4,'markerfacecolor','k')
    h = gca;
    h.FontSize = 8;
    if pIdx == 1
        ylabel('Resting PSD (a.u.)')
        text(-2.5,11,'A','fontsize',14,'fontweight','bold','HorizontalAlignment','center','VerticalAlignment','middle')
    end
    yticks([])
    xlim([5 40])
    xticks(5:5:40)
    xticklabels({'','10','','','','30 Hz','',''})
%     xticklabels({'','','','','','','',''})
    set(gca,'TickDir','out');
    title(sprintf('Patient %d', pIdx))
    box('off')
    if pIdx == 2
        text(45,24,'Resting','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',14,'fontweight','bold')
    end
    if pIdx == 4
        lhPSD = legend('A','M','P','location','northeast','orientation','vertical');
        legend('boxoff')
        lhPSD.Position(1) = .9;
    end

    % bar plots
    subaxis(2,4,pIdx+4,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    ax = gca; ax.FontSize = 7.5;
    tmpX = [squeeze(areaUnderCurve(pIdx,conditionColumns,2)); squeeze(areaUnderCurve(pIdx,conditionColumns,3)); squeeze(areaUnderCurve(pIdx,conditionColumns,4)); squeeze(areaUnderCurve(pIdx,conditionColumns,5))];
    tmpY = [squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,2)); squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,3)); squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,4)); squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,5))];
    hold off
    [HB, ~, ~, ~, ~] = superbar(tmpX,'E',tmpY,'BarFaceColor',barFaceColor,'ErrorbarRelativeWidth',0.33,'ErrorbarStyle','T','ErrorbarLineWidth',1.3);
    ylim([0 2.5])
    yticks(0:1:2)
    yticklabels({'0','1','2'})
    xlim([0.5 4.5])
    xticks([1-1/3.75 1 1+1/3.75 2-1/3.75 2 2+1/3.75 3-1/3.75 3 3+1/3.75 4-1/3.75 4 4+1/3.75])
    switch pIdx
        case 1
            xticklabels({'A' 'M' 'P' 'A' 'M' 'P' 'A' 'M' 'P' 'A' 'M' 'P'})
            ylabel('Power (a.u.)')
            text(-.5,2.5,'B','fontsize',14,'fontweight','bold','HorizontalAlignment','center','VerticalAlignment','middle')
        case 2
            xticklabels({'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M'})
        case 3
            xticklabels({'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P'})
        case 4
            xticklabels({'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P'})
    end
    title(sprintf('%s peak', patientPeakResting{pIdx}))
    set(gca,'TickDir','out');
    box off
    if pIdx == 4
        lhBar = legend(HB(:,1),{'\beta','\beta_{low}','\beta_{high}','\beta_{peak}'},'orientation','horizontal','location','southeast');
        legend('boxoff')
        lhBar.Position(1) = lhBar.Position(1)+.04;
        lhBar.Position(2) = -.0015;
        lhBar.FontSize = 8.5;
    end
end

%%
% Moving
conditionColumns = [1 2 3];
figure(fh03)
for pIdx = 1:length(patientNo)
    subaxis(2,4,pIdx,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    hold off
    plot(foi(13:end)',psdAll(13:end,1,pIdx),'linewidth',1.5,'color',lineColors(1,:))
    hold on
    plot(foi(13:end)',psdAll(13:end,2,pIdx),'linewidth',1.5,'color',lineColors(2,:))
    plot(foi(13:end)',psdAll(13:end,3,pIdx),'linewidth',1.5,'color',lineColors(3,:))
    plot([peakFreq(pIdx,1) peakFreq(pIdx,1)],[0 peakPSD(pIdx,1)],'k:','linewidth',1)
    plot(peakFreq(pIdx,1),peakPSD(pIdx,1),'k^','markersize',4,'markerfacecolor','k')
    h = gca;
    h.FontSize = 8;
    if pIdx == 1
        ylabel('Moving PSD (a.u.)')
        text(-2.5,11,'A','fontsize',14,'fontweight','bold','HorizontalAlignment','center','VerticalAlignment','middle')
    end
    yticks([])
    xlim([5 40])
    xticks(5:5:40)
    xticklabels({'','10','','','','30 Hz','',''})
    set(gca,'TickDir','out');
    title(sprintf('Patient %d', pIdx))
    box('off')
    if pIdx == 2
        text(45,24,'Moving','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',14,'fontweight','bold')
    end
    if pIdx == 4
        lhPSD = legend('A','M','P','location','northeast','orientation','vertical');
        legend('boxoff')
        lhPSD.Position(1) = .9;
    end

    % bar plots
    subaxis(2,4,pIdx+4,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    ax = gca; ax.FontSize = 7.5;
    tmpX = [squeeze(areaUnderCurve(pIdx,conditionColumns,2)); squeeze(areaUnderCurve(pIdx,conditionColumns,3)); squeeze(areaUnderCurve(pIdx,conditionColumns,4)); squeeze(areaUnderCurve(pIdx,conditionColumns,5))];
    tmpY = [squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,2)); squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,3)); squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,4)); squeeze(areaUnderCurveUpperBound(pIdx,conditionColumns,5))];
    hold off
    [HB, ~, ~, ~, ~] = superbar(tmpX,'E',tmpY,'BarFaceColor',barFaceColor,'ErrorbarRelativeWidth',0.33,'ErrorbarStyle','T','ErrorbarLineWidth',1.3);
    ylim([0 2.5])
    yticks(0:1:2)
    yticklabels({'0','1','2'})
    xlim([0.5 4.5])
    xticks([1-1/3.75 1 1+1/3.75 2-1/3.75 2 2+1/3.75 3-1/3.75 3 3+1/3.75 4-1/3.75 4 4+1/3.75])
    switch pIdx
        case 1
            xticklabels({'A' 'M' 'P' 'A' 'M' 'P' 'A' 'M' 'P' 'A' 'M' 'P'})
            ylabel('Power (a.u.)')
            text(-.5,2.5,'B','fontsize',14,'fontweight','bold','HorizontalAlignment','center','VerticalAlignment','middle')
        case 2
            xticklabels({'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M'})
        case 3
            xticklabels({'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P'})
        case 4
            xticklabels({'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P'})
    end
    title(sprintf('%s peak', patientPeakMoving{pIdx}))
    set(gca,'TickDir','out');
    box off
    if pIdx == 4
        lhBar = legend(HB(:,1),{'\beta','\beta_{low}','\beta_{high}','\beta_{peak}'},'orientation','horizontal','location','southeast');
        legend('boxoff')
        lhBar.Position(1) = lhBar.Position(1)+.04;
        lhBar.Position(2) = -.0015;
        lhBar.FontSize = 8.5;
    end
end

% A and B labels have to be aligned manually in these two Resting and
% Movign plots

%%
% Moving to resting, need to renormalize to same factor
conditionColumns = [1 2 3];
conditionColumns2 = [4 5 6];
figure(fh04)

for pIdx = 1:length(patientNo)
    subaxis(2,2,pIdx,'SpacingHoriz',spacingHor,'SpacingVert',spacingVer,'MR',marginRight,'ML',marginLeft)
    ax = gca; ax.FontSize = 7.5;
    tmpX = [areaUnderCurve(pIdx,conditionColumns,2)./(areaUnderCurve(pIdx,conditionColumns2,2)*normFactor(pIdx,2)/normFactor(pIdx,1));...
        areaUnderCurve(pIdx,conditionColumns,3)./(areaUnderCurve(pIdx,conditionColumns2,3)*normFactor(pIdx,2)/normFactor(pIdx,1));...
        areaUnderCurve(pIdx,conditionColumns,4)./(areaUnderCurve(pIdx,conditionColumns2,4)*normFactor(pIdx,2)/normFactor(pIdx,1));...
        areaUnderCurve(pIdx,conditionColumns,5)./(areaUnderCurve(pIdx,conditionColumns2,5)*normFactor(pIdx,2)/normFactor(pIdx,1))];
    hold off
    [HB, ~, ~, ~, ~] = superbar(tmpX,'BarFaceColor',barFaceColor);
    hold on
    plot([.6 4.4],[1 1],'k--','linewidth',1.2)
    ylim([0 2])
    yticks(0:.5:2.5)
    yticklabels({'0','','1','','2'})
    xlim([0.5 4.5])
    xticks([1-1/3.75 1 1+1/3.75 2-1/3.75 2 2+1/3.75 3-1/3.75 3 3+1/3.75 4-1/3.75 4 4+1/3.75])
    switch pIdx
        case 1
            xticklabels({'A' 'M' 'P' 'A' 'M' 'P' 'A' 'M' 'P' 'A' 'M' 'P'})
            ylabel('Ratio')
        case 2
            xticklabels({'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M'})
        case 3
            xticklabels({'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P'})
            ylabel('Ratio')
        case 4
            xticklabels({'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P' 'M' 'A' 'P'})
    end
    title(sprintf('Patient %d', pIdx))
    set(gca,'TickDir','out');
    box off
    if pIdx == 1
        text(5,2.4,'Moving-to-resting','HorizontalAlignment','center','VerticalAlignment','middle','fontsize',14,'fontweight','bold')
    end
    if pIdx == 4
        lhBar = legend(HB(:,1),{'\beta','\beta_{low}','\beta_{high}','\beta_{peak}'},'orientation','horizontal','location','southeast');
        legend('boxoff')
        lhBar.Position(1) = lhBar.Position(1)+.04;
        lhBar.Position(2) = -.0015;
        lhBar.FontSize = 8.5;
    end
end

%% printing
if doPrint
    print(fh02,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig02PatientRestingRev02'),'-dtiffn','-r300')
    print(fh03,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig03PatientMovingRev02'),'-dtiffn','-r300')
    print(fh04,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig04PatientMovingRestingRev02'),'-dtiffn','-r300')

    saveas(fh02,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig02PatientRestingRev02'),'epsc');
    saveas(fh03,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig03PatientMovingRev02'),'epsc');
    saveas(fh04,strcat('..\Figures\FrontiersHumanNeuroscience\Rev02\','Fig04PatientMovingRestingRev02'),'epsc');
end
