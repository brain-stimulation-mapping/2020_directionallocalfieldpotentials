# 2020_DirectionalLocalFieldPotentials

Nguyen et al. (2020), Directional local field potentials in the subthalamic nucleus during deep brain implantation of Parkinson's disease patients, https://doi.org/10.3389/fnhum.2020.521282

This repo has the analysis scripts only but not the data files to safeguard the patients' privacy. Please contact the main author, if you would like to have this. The analysis scripts are the final version used for the paper and the repo does not include the earlier revisions.
